package com.iv3d.sql;

import java.io.File;

import com.iv3d.sql.DbConnection;
import com.iv3d.sql.DbConnectionFactory;
import com.iv3d.sql.SqliteConnectionFactory;

import junit.framework.TestCase;

public class SqliteDbConnectionFactoryTest extends TestCase {
	private static final String DATA_ROOT = "TEST_DATA_ROOT";
	private static final String DB_FILE = "iv3d.projman.db";
	
	private final File dataRootDir = new File(DATA_ROOT);
	private final File dbFile = new File(DATA_ROOT, DB_FILE);
	private DbConnection db;
	
	@Override
	protected void setUp() throws Exception {
		super.setUp();
		db = null;
	}
	
	@Override
	protected void tearDown() throws Exception {
		super.tearDown();
		new File(DATA_ROOT, DB_FILE).delete();	
		new File(DATA_ROOT).delete();
		if(db!=null)
			db.close();
	}
	
	public void testConnect() {
		DbConnectionFactory instance = new SqliteConnectionFactory(DATA_ROOT, DB_FILE);
		assertFalse(dataRootDir.exists());
		db = instance.createConnection();
		assertTrue(dataRootDir.exists());
		assertTrue(dbFile.exists());
	}
}
