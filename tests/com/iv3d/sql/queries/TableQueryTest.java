package com.iv3d.sql.queries;

import com.iv3d.sql.SqlTestCase;

public class TableQueryTest extends SqlTestCase {
	private DbGetTableNames instance;
	
	@Override
	protected void setUp() throws Exception {
		super.setUp();
		
		instance = new DbGetTableNames();
	}
	
	public void testSql() {
		assertEquals("SELECT name FROM sqlite_master WHERE type='table'",
				instance.getSql());
	}
	
	public void testParse() {
		updateSql("DROP TABLE IF EXISTS `test_table2`;");
		updateSql("DROP TABLE IF EXISTS `test_table1`;");
		
		updateSql("CREATE TABLE `test_table1`  (id INTEGER);");
		updateSql("CREATE TABLE `test_table2`  (id INTEGER);");
		
		String[] tables = querySql(instance);
		
		updateSql("DROP TABLE `test_table1`;");
		updateSql("DROP TABLE `test_table2`;");
		
		assertEquals(2, tables.length);
		assertEquals("test_table1", tables[0]);
		assertEquals("test_table2", tables[1]);
	}
	

}
