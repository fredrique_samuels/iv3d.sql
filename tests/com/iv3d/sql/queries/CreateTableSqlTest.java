package com.iv3d.sql.queries;

import com.iv3d.sql.DbUpdate;
import com.iv3d.sql.SqliteColumnType;
import com.iv3d.sql.SqliteField;
import com.iv3d.sql.SqliteFieldDef;

import junit.framework.TestCase;

public class CreateTableSqlTest extends TestCase {
	
	public void testNoFields() {
		DbUpdate sql = new DbCreateTable("table_name");
		assertEquals("CREATE TABLE `table_name`;", sql.getSql());
	}
	
	public void testTextField()  {
		SqliteField fieldDef = new SqliteFieldDef("field1", SqliteColumnType.TEXT);
		DbUpdate sql = new DbCreateTable("test_table")
				.with(fieldDef);
		
		assertEquals(
				"CREATE TABLE `test_table` ("
				+ "`field1` TEXT"
				+ ");", 
				sql.getSql());
	}
	
	public void testBlobField()  {
		SqliteField fieldDef = new SqliteFieldDef("field1", SqliteColumnType.BLOB);
		DbUpdate sql = new DbCreateTable("test_table")
				.with(fieldDef);
		
		assertEquals(
				"CREATE TABLE `test_table` ("
				+ "`field1` BLOB"
				+ ");", 
				sql.getSql());
	}
	
	public void testNumaricField()  {
		SqliteField fieldDef = new SqliteFieldDef("field1", SqliteColumnType.NUMERIC);
		DbUpdate sql = new DbCreateTable("test_table")
				.with(fieldDef);
		
		assertEquals(
				"CREATE TABLE `test_table` ("
				+ "`field1` NUMERIC"
				+ ");", 
				sql.getSql());
	}
	
	public void testRealField()  {
		SqliteField fieldDef = new SqliteFieldDef("field1", SqliteColumnType.REAL);
		DbUpdate sql = new DbCreateTable("test_table")
				.with(fieldDef);
		
		assertEquals(
				"CREATE TABLE `test_table` ("
				+ "`field1` REAL"
				+ ");", 
				sql.getSql());
	}
	
	public void testMultipleFieldsField()  {
		DbUpdate sql = new DbCreateTable("test_table")
				.with(new SqliteFieldDef("field1", SqliteColumnType.REAL))
				.with(new SqliteFieldDef("field2", SqliteColumnType.TEXT));
		
		assertEquals(
				"CREATE TABLE `test_table` ("
				+ "`field1` REAL, "
				+ "`field2` TEXT"
				+ ");", 
				sql.getSql());
	}
	
	public void testNotNull()  {
		SqliteField fieldDef = new SqliteFieldDef("field1", SqliteColumnType.REAL)
				.setNotNull();
		DbUpdate sql = new DbCreateTable("test_table")
				.with(fieldDef);
		
		assertEquals(
				"CREATE TABLE `test_table` ("
				+ "`field1` REAL NOT NULL"
				+ ");", 
				sql.getSql());
	}
	
	public void testUnique()  {
		SqliteField fieldDef = new SqliteFieldDef("field1", SqliteColumnType.REAL)
				.setUnique();
		DbUpdate sql = new DbCreateTable("test_table")
				.with(fieldDef);
		
		assertEquals(
				"CREATE TABLE `test_table` ("
				+ "`field1` REAL UNIQUE"
				+ ");", 
				sql.getSql());
	}
	
	public void testDefault()  {
		SqliteField fieldDef = new SqliteFieldDef("field1", SqliteColumnType.REAL)
				.setDefault("33");
		DbUpdate sql = new DbCreateTable("test_table")
				.with(fieldDef);
		
		assertEquals(
				"CREATE TABLE `test_table` ("
				+ "`field1` REAL DEFAULT 33"
				+ ");", 
				sql.getSql());
	}
	
	public void testWithRowId() {
		DbCreateTable sql = new DbCreateTable("table_name")
				.withRowId();
		assertEquals("CREATE TABLE `table_name` (`id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE);", sql.getSql());
	}
	
	public void testIfNotExsts()  {
		DbUpdate sql = new DbCreateTable("test_table")
				.setIfNotExists();
		
		assertEquals(
				"CREATE TABLE IF NOT EXISTS `test_table`;", 
				sql.getSql());
	}
	
}
