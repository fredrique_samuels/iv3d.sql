package com.iv3d.sql;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import junit.framework.TestCase;
import mockit.Mocked;
import mockit.NonStrictExpectations;
import mockit.Verifications;
import mockit.VerificationsInOrder;

public class SqlDbConnectionTest extends TestCase {
	private SqliteDbConnection instance; 
	@Mocked 
	Connection connection;
	@Mocked 
	Statement statement;
	@Mocked
	private DbUpdate dbUpdate;
	@Mocked
	private DbQuery<String> dbQuery;
	@Mocked
	protected ResultSet resultSet;
	
	@Override
	protected void setUp() throws Exception {
		super.setUp();
		new NonStrictExpectations() {
			{
				connection.createStatement();
				result = statement;
			}
		};
		instance = new SqliteDbConnection(connection);
	}
	
	public void testCreate() {
		new SqliteDbConnection(connection);
	}
	
	public void testClose() throws SQLException {
		instance.close();
		new Verifications() {
			{
				connection.close();
				times = 1;
			}
		};
	}
	
	public void testIsClosed() throws SQLException {
		new NonStrictExpectations() {
			{
				connection.isClosed();
				result = true;
			}
		};
		
		SqliteDbConnection instance = new SqliteDbConnection(connection);
		assertTrue(instance.isClosed());
		
		new Verifications() {
			{
				connection.isClosed();
				times = 1;
			}
		};
	}
	
	public void testUpdate() throws SQLException {
		
		new NonStrictExpectations() {
			{
				statement.executeUpdate(anyString);
				result = 1;
			}
		};
		
		instance.executeUpdate(dbUpdate);
		
		new VerificationsInOrder() {
			{
				connection.createStatement();
				times=1;
				
				dbUpdate.getSql();
				times=1;
				
				statement.executeUpdate(anyString);
				times=1;
				
				connection.commit();
				times=1;

				statement.close();
				times=1;
				
			}
		};
	}
	
	public void testStmtClosedWhenSqlExceptionOccurres() throws SQLException {
		new NonStrictExpectations() {
			{
				statement.executeUpdate(anyString);
				result = new SQLException();
			}
		};
		
		try {			
			instance.executeUpdate(dbUpdate);
			fail();
		} catch (SqliteDbConnection.SqlException e) {
			
		}
		
	}
	
	public void testQuery() throws SQLException {
		
		new NonStrictExpectations() {
			{
				statement.executeQuery(anyString);
				result = resultSet;
				
				dbQuery.parse(resultSet);
				result = "value";
			}
		};
		
		String value = instance.executeQuery(dbQuery);
		assertEquals("value", value);
		
		new VerificationsInOrder() {
			{
				connection.createStatement();
				times=1;
				
				dbQuery.getSql();
				times=1;
				
				statement.executeQuery(anyString);
				times=1;

				dbQuery.parse(resultSet);
				times=1;
				
				resultSet.close();
				times=1;
				
				statement.close();
				times=1;
			}
		};
	}
	
	public void testSqlExceptionOnError() throws SQLException {
		new NonStrictExpectations() {
			{
				statement.executeQuery(anyString);
				result = new SQLException();
				
			}
		};
		
		try {
			instance.executeQuery(dbQuery);
			fail();
		} catch (SqliteDbConnection.SqlException e) {
			
		}
		
		new VerificationsInOrder() {
			{				
				statement.close();
				times=1;
			}
		};
	}
	
	public void testErrorOnDbQueryParseInvocation() throws SQLException {
		new NonStrictExpectations() {
			{
				statement.executeQuery(anyString);
				result = resultSet;
				
				dbQuery.parse(resultSet);
				result = new RuntimeException();
			}
		};
		
		try {
			instance.executeQuery(dbQuery);
			fail();
		} catch (SqliteDbConnection.DbQueryParseError e) {
			
		}
		
		new VerificationsInOrder() {
			{	
				resultSet.close();
				times=1;
				
				statement.close();
				times=1;
			}
		};
	}
}
