package com.iv3d.sql.db;

import java.sql.Connection;
import java.sql.DriverManager;

import com.iv3d.sql.DbConnectionFactory;

import junit.framework.TestCase;
import mockit.Mocked;
import mockit.NonStrictExpectations;

public class DefaultSqlManagerTest extends TestCase {
	private static final String ADMIN_TABLE = "IV3D_PROJMAN_ADMIN";
	
	@Mocked
	private DbConnectionFactory connectionFactory;
	@Mocked
	Connection connection;
	
	private DbManager sqlManager = null;

	@Override
	protected void setUp() throws Exception {
		super.setUp();
	}

	private DbConnectionFactory getMemoryDbFactory() {
		new NonStrictExpectations() {
			{
				connectionFactory.createConnection();
				result = createConnection();
			}

			private Connection createConnection() {
				try {
					Class.forName("org.sqlite.JDBC");
					return DriverManager.getConnection("jdbc:sqlite:memory:");
			    } catch ( Exception e ) {
			    	throw new RuntimeException(e);
			    }
			}
		};
		
		return connectionFactory;
	}
	
	@Override
	protected void tearDown() throws Exception {
		super.tearDown();
	}
	
	public void testConnect() {
		
	}
	
	public void testSchemaUpdatesWithNoConnectionThrowsError() {
		try {
			sqlManager.runDbSchemaUpdates();
			fail();
		} catch (DefaultDbManager.NoSqlConnectionError e) {
			
		}
	}
	
	public void testVersionTableCreated() {
		DbConnectionFactory factory = getMemoryDbFactory();
		sqlManager = new DefaultDbManager(factory, ADMIN_TABLE);
		
	}
	
}
