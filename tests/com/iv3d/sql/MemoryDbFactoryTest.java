package com.iv3d.sql;

import java.sql.SQLException;

import junit.framework.TestCase;

public class MemoryDbFactoryTest extends TestCase {
	public void testCreate() throws SQLException {
		SqliteMemoryConnectionFactory factory = new SqliteMemoryConnectionFactory();
		DbConnection connection=null;
		try {
			connection = factory.createConnection();
			assertNotNull(connection);
			assertFalse(connection.isClosed());
		} finally {
			if(connection!=null)
				connection.close();
		}
		
	}
}
