package com.iv3d.sql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.iv3d.sql.DbConnectionFactory;
import com.iv3d.sql.DbQuery;

import junit.framework.TestCase;
import mockit.Mocked;
import mockit.NonStrictExpectations;

public class SqlTestCase extends TestCase {
	
	@Mocked
	private DbConnectionFactory connectionFactory;
	private Connection connection;
	
	@Override
	protected void setUp() throws Exception {
		super.setUp();
		new NonStrictExpectations() {
			{
				connectionFactory.createConnection();
				result = createConnection();
			}
		};
	}
	
	@Override
	protected void tearDown() throws Exception {
		super.tearDown();
		if(connection!=null)
			connection.close();
		connection = null;
	}
	
	private Connection createConnection() {
		if(connection!=null) {
			return connection;
		}
		
		try {
			Class.forName("org.sqlite.JDBC");
			connection = DriverManager.getConnection("jdbc:sqlite:memory:");
			connection.setAutoCommit(false);
	    } catch ( Exception e ) {
	    	throw new RuntimeException(e);
	    }
		return connection;
	}
	
	protected final void updateSql(String sql) {
		try {
			Statement stmt = connection.createStatement();
			stmt.executeUpdate(sql);
			connection.commit();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}
	
	protected final <T> T querySql(DbQuery<T> query) {
		Statement stmt=null;
		ResultSet result=null;
		try {
			stmt = connection.createStatement();
			result = stmt.executeQuery(query.getSql());
			T parse = query.parse(result);
			return parse;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			closeResult(result);
			closeStmt(stmt);
		}
	}
	
	private void closeStmt(Statement stmt) {
		try {
			stmt.close();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	protected final void closeResult(ResultSet resultSet) {
		if(resultSet==null) {
			return;
		}
		try {
			resultSet.close();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}
	
	public void testNothing(){}
}
