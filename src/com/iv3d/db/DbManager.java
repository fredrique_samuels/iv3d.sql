package com.iv3d.db;

import com.iv3d.sql.DbQuery;

public interface DbManager {
	void close();
	void runDbSchemaUpdates();
	<T> T execute(DbQuery<T> query);
}
