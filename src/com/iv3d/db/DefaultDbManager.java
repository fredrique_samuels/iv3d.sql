package com.iv3d.db;

import com.iv3d.sql.DbConnection;
import com.iv3d.sql.DbConnectionFactory;
import com.iv3d.sql.DbQuery;

public class DefaultDbManager implements DbManager {

	private DbConnection connection = null;
	private String adminTable;
	private DbConnectionFactory connectionFactory;

	public DefaultDbManager(DbConnectionFactory connectionFactory, String adminTable2) {
		this.adminTable = adminTable;
		this.connectionFactory = connectionFactory;
		this.connection = connectionFactory.createConnection();
	}

	@Override
	public void runDbSchemaUpdates() {
		if(connection==null) { 
			throw new NoSqlConnectionError();
		}
	}

	@Override
	public synchronized void close() {
		if(connection!=null)
			closeSql();
	}
	
	@Override
	public <T> T execute(DbQuery<T> query) {
		return null;
	}

	private void closeSql() {
		connection.close();
		connection=null;
	}

	public class NoSqlConnectionError extends RuntimeException {
		private static final long serialVersionUID = 1L;
		
		public NoSqlConnectionError() {
			super("Sql Connection not available. Did you forgat to call connect()?");
		}
	}
}
