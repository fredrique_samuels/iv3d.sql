package com.iv3d.sql;

import java.sql.Connection;
import java.sql.DriverManager;

public class SqliteMemoryConnectionFactory implements DbConnectionFactory {

	@Override
	public DbConnection createConnection() {
		try {
			Class.forName("org.sqlite.JDBC");
			Connection connection = DriverManager.getConnection("jdbc:sqlite:memory:");
			return new SqliteDbConnection(connection);
	    } catch ( Exception e ) {
	    	throw new SqlConnectFailedError(e);
	    }
	}
	
	public class SqlConnectFailedError extends RuntimeException {
		private static final long serialVersionUID = 1L;
		
		public SqlConnectFailedError(Exception e) {
			super(e);
		}
	}

}
