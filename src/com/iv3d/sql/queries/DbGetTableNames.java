package com.iv3d.sql.queries;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.iv3d.sql.DbQuery;

public class DbGetTableNames implements DbQuery<String[]> {

	@Override
	public String getSql() {
		return "SELECT name FROM sqlite_master WHERE type='table'";
	}

	@Override
	public String[] parse(ResultSet resultSet) {
		ArrayList<String> list = new ArrayList<String>();
		try {
			while(resultSet.next()) {
				list.add(resultSet.getString("name"));
			}
		} catch (SQLException e) {
			throw new ResultParseError(e);
		}
		return list.toArray(new String[]{});
	}
	
}
