package com.iv3d.sql.queries;

import java.util.ArrayList;
import java.util.List;

import com.iv3d.sql.DbUpdate;
import com.iv3d.sql.SqliteField;

public final class DbCreateTable implements DbUpdate {

	private String tableName;
	private List<SqliteField> fields;
	private boolean withRowId;
	private boolean ifNotExists;

	public DbCreateTable(String tableName) {
		this.tableName = tableName;
		this.fields = new ArrayList<SqliteField>();
		this.withRowId = false;
		this.ifNotExists = false;
	}

	@Override
	public final String getSql() {
		StringBuilder builder = new StringBuilder();
		builder.append("CREATE TABLE ");
		builder.append(getIfNotExists());
		builder.append(String.format("`%s`", tableName));
		if(fields.size()>0 || withRowId) {
			builder.append(" (");
			builder.append(getFields());
			builder.append(")");
		};
		builder.append(";");
		
		return builder.toString();
	}

	private String getIfNotExists() {
		if(ifNotExists) {
			return "IF NOT EXISTS ";
		}
		return "";
	}

	private String getFields() {
		List<String> fieldStrings = new ArrayList<String>();
		
		String rowId = getRowIdField();
		if(!rowId.isEmpty()) fieldStrings.add(rowId);

		for (SqliteField field : fields) {
			fieldStrings.add(getFieldSql(field));
		}
		
		return String.join(", ", fieldStrings.toArray(new String[]{}));
	}

	private String getRowIdField() {
		if(withRowId) {
			return "`id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE";
		}
		return "";
	}

	public final DbCreateTable with(SqliteField fieldDef) {
		fields.add(fieldDef);
		return this;
	}

	private String getFieldSql(SqliteField field) {
		StringBuilder builder = new StringBuilder();
		builder.append(getBaseField(field));
		builder.append(getNotNullString(field));
		builder.append(getUniqueString(field));
		builder.append(getDefaultString(field));
		return builder.toString();
	}

	private String getDefaultString(SqliteField field) {
		if(field.getDefault()==null) {
			return "";
		}
		return String.format(" DEFAULT %s", field.getDefault());
	}

	private String getBaseField(SqliteField field) {
		return String.format("`%s` %s", field.getName(), field.getType().toString());
	}

	private String getUniqueString(SqliteField field) {
		return field.unique()?" UNIQUE":"";
	}

	private String getNotNullString(SqliteField field) {
		return field.notNull()?" NOT NULL":"";
	}

	public DbCreateTable withRowId() {
		withRowId = true;
		return this;
	}

	public DbCreateTable setIfNotExists() {
		ifNotExists = true;
		return this;
	}
	
}
