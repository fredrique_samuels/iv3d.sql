package com.iv3d.sql;

public interface SqliteField {
	String getName();
	SqliteColumnType getType();
	boolean notNull();
	boolean unique();
	String getDefault();
}
