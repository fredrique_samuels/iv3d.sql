package com.iv3d.sql;

public enum SqliteColumnType {
	TEXT, BLOB, NUMERIC, REAL
}
