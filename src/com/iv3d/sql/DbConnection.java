package com.iv3d.sql;

public interface DbConnection {
	boolean isClosed();
	void close();
}