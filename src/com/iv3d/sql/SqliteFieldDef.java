package com.iv3d.sql;

public final class SqliteFieldDef implements SqliteField {

	private String name;
	private SqliteColumnType type;
	private boolean notNull;
	private boolean unique;
	private String defaultValue;

	public SqliteFieldDef(String name, SqliteColumnType type) {
		this.name = name;
		this.type = type;
		this.notNull=false;
		this.unique=false;
	}

	@Override
	public final String getName() {
		return name;
	}

	@Override
	public final SqliteColumnType getType() {
		return type;
	}

	public final SqliteFieldDef setNotNull() {
		notNull = true;
		return this;
	}

	@Override
	public boolean notNull() {
		return notNull;
	}

	public SqliteFieldDef setUnique() {
		unique=true;
		return this;
	}

	@Override
	public boolean unique() {
		return unique;
	}

	public final SqliteFieldDef setDefault(String value) {
		defaultValue = value;
		return this;
	}

	@Override
	public String getDefault() {
		return defaultValue;
	}

}
