package com.iv3d.sql;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;

public class SqliteConnectionFactory implements DbConnectionFactory {

	private String dataRoot;
	private String dbFile;

	public SqliteConnectionFactory(String dataRoot, String dbFile) {
		this.dataRoot = dataRoot;
		this.dbFile = dbFile;
	}

	@Override
	public DbConnection createConnection() {
		new File(dataRoot).mkdirs();
		File file = new File(dataRoot, dbFile);
	    try {
	      Class.forName("org.sqlite.JDBC");
	      Connection connection = DriverManager.getConnection("jdbc:sqlite:"+file.getPath());
	      connection.setAutoCommit(false);
		return new SqliteDbConnection(connection);
	    } catch ( Exception e ) {
	    	throw new SqlConnectFailedError(e);
	    }
	}
	
	public class SqlConnectFailedError extends RuntimeException {
		private static final long serialVersionUID = 1L;
		
		public SqlConnectFailedError(Exception e) {
			super(e);
		}
	}

}
