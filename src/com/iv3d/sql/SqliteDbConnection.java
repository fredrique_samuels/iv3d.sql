package com.iv3d.sql;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

class SqliteDbConnection implements DbConnection {

	private final Connection connection;

	public SqliteDbConnection(Connection connection) {
		this.connection = connection;
	}

	@Override
	public boolean isClosed() {
		try {
			return connection.isClosed();
		} catch (SQLException e) {
			throw new SqlException(e); 
		}
	}

	@Override
	public void close() {
		try {
			connection.close();
		} catch (SQLException e) {
			throw new SqlException(e);
		}
	}

	public void executeUpdate(DbUpdate updateSql) {
		Statement stmt=null;
		try {
			stmt = connection.createStatement();
			stmt.executeUpdate(updateSql.getSql());
			connection.commit();
		} catch (SQLException e) {
			throw new SqlException(e);
		} finally {
			closeStatement(stmt);
		}
	}

	private void closeStatement(Statement stmt) {
		if(stmt!=null)
			try {
				stmt.close();
			} catch (SQLException e) {
				//ignore
			}
	}

	public <T> T executeQuery(DbQuery<T> dbQuery) {
		Statement stmt=null;
		try {
			stmt = connection.createStatement();
			ResultSet result = stmt.executeQuery(dbQuery.getSql());
			try {
				return dbQuery.parse(result);
			} catch (Exception e) {
				throw new DbQueryParseError(e);
			} finally {
				result.close();
			}
		} catch (SQLException e) {
			throw new SqlException(e);
		} finally {
			closeStatement(stmt);
		}
	}
	
	public class SqlException extends RuntimeException {
		private static final long serialVersionUID = 1L;
		public SqlException(SQLException e) {
			super(e);
		}
	}
	
	public class DbQueryParseError extends RuntimeException {

		private static final long serialVersionUID = 1L;

		public DbQueryParseError(Exception e) {
			super(e);
		}
	}
}
