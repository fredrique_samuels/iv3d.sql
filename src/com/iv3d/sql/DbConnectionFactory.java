package com.iv3d.sql;

public interface DbConnectionFactory {
	DbConnection createConnection();
}
