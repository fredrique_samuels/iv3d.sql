package com.iv3d.sql;

import java.sql.ResultSet;

public interface DbQuery<T> {
	String getSql();
	T parse(ResultSet resultSet);
	
	public final class ResultParseError extends RuntimeException {
		private static final long serialVersionUID = 1L;
		
		public ResultParseError(Exception e) {
			super(e);
		}
	}
}
